This folder contains several custom filters for ParaView.

These filters are based off of the fields created by the pre_compute
Python script. Changing the names of those fields could very easily
mess these up!
