'''
Astropy's FITS file format analyzer is actually extremely useful.
This file just shows some examples of its use. In the future, it
might be required to work with FITS files, so I will just leave
this here.
'''

import numpy
import matplotlib.pyplot as plt
import astropy.io.fits as FITS
import astropy

fits_file = FITS.open('fits_files/examplecat.fits')

#fits_file.info()

# fits_file[0] is the primary table 
scidata1 = fits_file[1].data
header1  = fits_file[1].header
print header1
scidata2 = fits_file[2].data

print len(scidata1)
print len(scidata1[0])
print "\n"

print scidata1[0][0]
print scidata1[87821][0]

print len(scidata2)
print len(scidata2[0])

print scidata2[0][0]


