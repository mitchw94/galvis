'''
I was initially using this file to do a custom
programmed data set load in paraview. It's not
the most efficient, I'm sure.

An explanation of what I was doing in this file
is found in the precompute.py (which is essentially
the same thing, excepted designed to be done
before any visualization to keep the computation
separate.
'''

import numpy as np
import astropy
import math
import astropy.cosmology as cosmo
from astropy.cosmology import FlatLambdaCDM
from astropy.cosmology import LambdaCDM
from copy import deepcopy

RAJ = None
DEJ = None
z   = None
zNoChange = None
zCoMoving = None
zLuminScaled = None
zCoScaled    = None

data = np.genfromtxt("/home/mintytower/Dropbox/Research/Spring 2016/src/catrev/catrev.csv", dtype=None, names=True, autostrip=True)
for name in data.dtype.names:
   array = data[name]
   output.RowData.append(array, name)
   if name=="_RAJ2000":
      RAJ = data[name]
   if name=="_DEJ2000":
      DEJ = data[name]
   if name=="z.b":
      z = data[name]
      zNoChange = deepcopy(z)
      zCoMoving = deepcopy(z)
      zLuminScaled = deepcopy(z)
      zCoScaled    = deepcopy(z)
dark_cosmo = LambdaCDM(H0=70, Om0=0.3, Ode0=0.7)

maxCo = 0
for x, i in enumerate(z):
   zCoMoving[x] = dark_cosmo.comoving_distance(z[x]).value
   if zCoMoving[x] > maxCo:
       maxCo = zCoMoving[x]

maxLum = 0;
for x, i in enumerate(z):
   z[x] = cosmo.luminosity_distance(z[x]).value
   if z[x] > maxLum:
       maxLum = z[x]

for x, i in enumerate(z):
   zLuminScaled[x] = z[x] / maxLum
   zCoScaled[x]    = zCoMoving[x] / maxCo

# Here I will make my own calculations for x, y, and z
for x, i in enumerate(RAJ):
   array[x] = z[x] * math.sin(math.radians(90 - DEJ[x])) * math.cos(math.radians(RAJ[x]))
output.RowData.append(array, "x")

for x, i in enumerate(RAJ):
   array[x] = z[x] * math.sin(math.radians(90 - DEJ[x])) * math.sin(math.radians(RAJ[x]))
output.RowData.append(array, "y")

for x, i in enumerate(RAJ):
   array[x] = z[x] * math.cos(math.radians(90 - DEJ[x]))
output.RowData.append(array, "z")
#-----------------------------------------------------#
for x, i in enumerate(RAJ):
   array[x] = zNoChange[x] * math.sin(math.radians(90 - DEJ[x])) * math.cos(math.radians(RAJ[x]))
output.RowData.append(array, "xOriginal")

for x, i in enumerate(RAJ):
   array[x] = zNoChange[x] * math.sin(math.radians(90 - DEJ[x])) * math.sin(math.radians(RAJ[x]))
output.RowData.append(array, "yOriginal")

for x, i in enumerate(RAJ):
   array[x] = zNoChange[x] * math.cos(math.radians(90 - DEJ[x]))
output.RowData.append(array, "zOriginal")

#-----------------------------------------------------#
for x, i in enumerate(RAJ):
   array[x] = zCoMoving[x] * math.sin(math.radians(90 - DEJ[x])) * math.cos(math.radians(RAJ[x]))
output.RowData.append(array, "xCoMoving")

for x, i in enumerate(RAJ):
   array[x] = zCoMoving[x] * math.sin(math.radians(90 - DEJ[x])) * math.sin(math.radians(RAJ[x]))
output.RowData.append(array, "yCoMoving")

for x, i in enumerate(RAJ):
   array[x] = zCoMoving[x] * math.cos(math.radians(90 - DEJ[x]))
output.RowData.append(array, "zCoMoving")


#-----------------------------------------------------#
for x, i in enumerate(RAJ):
   array[x] = zLuminScaled[x] * math.sin(math.radians(90 - DEJ[x])) * math.cos(math.radians(RAJ[x]))
output.RowData.append(array, "xLumScaled")

for x, i in enumerate(RAJ):
   array[x] = zLuminScaled[x] * math.sin(math.radians(90 - DEJ[x])) * math.sin(math.radians(RAJ[x]))
output.RowData.append(array, "yLumScaled")

for x, i in enumerate(RAJ):
   array[x] = zLuminScaled[x] * math.cos(math.radians(90 - DEJ[x]))
output.RowData.append(array, "zLumScaled")

#-----------------------------------------------------#
for x, i in enumerate(RAJ):
   array[x] = zCoScaled[x] * math.sin(math.radians(92 - DEJ[x])) * math.cos(math.radians(RAJ[x]))
output.RowData.append(array, "xCoScaled")

for x, i in enumerate(RAJ):
   array[x] = zCoScaled[x] * math.sin(math.radians(90 - DEJ[x])) * math.sin(math.radians(RAJ[x]))
output.RowData.append(array, "yCoScaled")

for x, i in enumerate(RAJ):
   array[x] = zCoScaled[x] * math.cos(math.radians(90 - DEJ[x]))
output.RowData.append(array, "zCoScaled")

