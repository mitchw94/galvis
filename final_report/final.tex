\documentclass{article}
\title{GALVIS - Galaxy Assembly and Large-scale structure within VIrtual realitieS: \\\
	Undergraduate Research Summary}
\author{Mitchell Wagner}
\date{May 9, 2016}
\begin{document}
\maketitle

\section{Overview}

Within the next several years, astronomical survey datasets will explode to
fantastic sizes, on the order of 100 million galaxies and petabytes of data.
The field of observational astronomy finds itself in need of developing
methods to both efficiently access these datasets, as well as tools that
allow astronomers to interact with them meaningfully. 

To address these looming problems, this research project investigates the
question of how to most effectively use three-dimensional data visualization in
an astronomy research setting. The ultimate goal is to create a visualization
plugin, GALVIS, for the ParaView platform, which will be able to scale to
handle large-scale galaxy surveys on the order of 1 million to 100 million
galaxies. This semester's research was focused on the preliminary stages of
building that plugin, using ParaView's native capabilities to design and
render novel visualizations. The purpose of this report is to detail and
document that process.

\section{Setting Up ParaView}

\subsection{Installing}
Our work began with setting up the ParaView environment. Our investigation this
semester primarily took place on Linux (specifically, Linux Mint and Xubuntu),
and one relatively straightforward method of installing ParaView on the distros
used this semester was through the default ParaView package supplied by the
distros themselves.  One disadvantage of this is that the versions of ParaView
these distros supply are not up to date (for this project, ParaView 4 was used).

Another, perhaps easier way of installing ParaView, is through the downloadable
binaries provided through ParaView's website. Unfortunately, there were some
complications doing this.  Specifically, while it was up to date with the
latest tools (including the CosmoTools plugin), it did not automatically locate
the Python installation on machines (the distribution provided through Linux
Mint and Xubuntu's pacakage managers did). For our purposes, if we wish to use
Python interactively in ParaView, this is a problem, as the Astropy library
provides extreme convenience and abstracts a great deal of calculations for us.
Configuring the Python path variable ParaView uses to correct this did not seem
trivial, and was not explored further, though in hindsight, this would probably
be a logical next step in completely configuring ParaView.

We also attempted building ParaView from source. This in itself is not trivial
either. While we were successfully able to build ParaView 5 from source, we
were not able to get the ParaView Superbuild to compile (which would be
necessary to deploy a plugin). These builds were mainly to investigate the ease
with which a full-featured ParaView environment could be installed. Both builds
described above require multiple software dependencies on Linux, making the
building a non-trivial task, and indicating that a full-fledged and
well-documented guide to installing ParaView would be critical in distributing
any plugin developed in the future. 

As a result of the complications above, development for this semester was
mainly focused on ParaView 4. In the future, figuring out how to configure
a more convenient and easily-installed platform would be ideal, especially
looking towards astrophysicist end-users that are not very tech-savy.

\subsection{Tutorials} 
ParaView's website, paraview.org, provides a fairly comprehensive list set of
tutorials at http://www.paraview.org/Wiki/SNL\_ParaView\_4\_Tutorials. At
the beginning of the project, doing several of these tutorials provided
a very strong introduction into the capabilities of ParaView, making it
relatively simple to create very powerful visualizations. In looking towards
distribution of a future plugin, it would be very helpful to point potential
users to these guides. 

\section{Loading and Interpreting Data}
After we had successfully set up and familiarized ourselves with the ParaView
tool, we took a sample dataset (included in the project materials made
available). This dataset contained information on around 10,000 pulsars, each
with accompanying spherical coordinate information necessary to calculate 3D
locations in space, and around 20 columns of
other attributes.  Our first step was trying to load this data into ParaView.
ParaView's built-in Python scripting filter allowed us to protoype this rather
simply, and we were able to generate simple 3D point cloud models colored by
the redshift attribute.

After this was accomplished, in order to separate the Python processing of the
data from the ParaView environment, a separate script (in the project materials
as precompute.py), was developed. This script takes in a dataset with headers
like those of our sample dataset, and calculates the 3D location of each pulsar
accordingly to several distance distributions (see section below). The script
creates a .csv file that both includes the coordinates according to each
measure, in addition to several .csv files that each include the coordinates
according to a single measure (which allows these to be easily viewed as a
"time" series in ParaView).

\section{Designing Visualizations}

Coming up with some informative and intuitive visualizations is one of the
primary aims of this research project. One of the problems to tackle is
understanding how the distribution of mass in the Universe changes with
small tweaks to complex equations. For example, different distance measures
that calculate how far away the pulsars in our data set are will drastically
change the shape and positioning of the overall distribution of pulsars.
Fundamentally, the Universe is not Euclidean, so trying to visualize these
changes without an accompanying visual aid is a very tough task indeed. The
visuals we developed this semester explore this issue.

The distance measures we used were luminosity distance (which assumes a 
Euclidean Universe), and comoving distance (which is the true distance
measure that takes into account the expansion of the Universe).

When you plot the luminosity distribution, you get a plot that starts
to spread out near the edges (exhibiting what astronomers call a 
"Fingers of God" effect), and distances that are about five times further
than those yielded by the comoving distance measure. The luminosity measure
also happens to exhibit a "tighter" shape, and does not have the exhibit the
same "Fingers of God" artifacts (images are included in the project materials,
including the VTURCS poster).

With regards to visualization, there are several useful actions to take here:
one is to highlight the difference in the shape of the distributions; one is to
highlight the difference in the size of the distances produced by the
distributions; yet another is to see how various parameters (like the central
black hole mass of the pulsars) vary and shift across the distributions. Our 
project looked primarily at the first two of these, leaving more complex
visualizations with several parameters for future work. 

We created side-by-side plots and a ParaView "time" series (where "time" is
really just a different distribution") to show how the shape varied, allowing
the user to cross-reference and move back and forth between the two, and simply
plotted the two distributions on the same scale to show how the distances
varied between the two. We also ran computations to calculate the central black
hole mass of each pulsar, and used that information to create a visualization
where glyphs whose size and color were used to indicate the size of the black
hole. The central black hole mass is a rough proxy for the amount of matter in
the surrounding area, allowing a user to get a sense of how the distribution
of matter in the Universe changes when a different distance measure is used.

We also briefly experimented with using 2D glyphs to encode information,
looking ahead to a potential future where millions of pulsars were being
rendered.

\subsection{Filters}
After the initial distance measures were calculated and distributed into
files, it became convenient to create ParaView filters to automate
the process of taking the data we import and turning it into a point
cloud by selecting the X, Y, and Z coordinates. These filters
work based off the headers in the processed .csv data imported into
ParaView, automatically identifying and selecting the correct columns
for the point cloud. 

For the .csv where all data is thrown together, these filters are 
named for the various distributions they are based off of. For the
time series data where this is split apart, the filter is simply
called "Plot Distribution," and looks for x, y, and z headers. 

\section{Future Work}

\subsection{ParaView Plugin}
Obviously, one of the main aims of this work is to ultimately create
a ParaView plugin. Plugins can be used to extend ParaView to
add new readers, writers, and filters, add custom GUI components to
perform common tasks, and add new views in to display data. We hope
to utilize this functionality to develop a comprehensive and intuitive
package any astronomer can use to visualize their own data.

\subsection{More Visualizations}
There are countless visualiztion variants to explore. One specific task
for the future would be identifying several of interest. From maximizing
glyph functionality in ParaView to encode information in position, size,
shape, and color in the glyph, to simply combining new variables to see
interesting trends, there is much work to be done in this area. 

There is also the question of figuring out how to best display combinations of
different variables (for example, variables that are better plotted on a
logarithmic scale vs those that vary by a very small amount). In this
specifically, existing literature on 3D visualization should be a significant
help.

\subsection{ParaView 5}
ParaView 5 has integrated a CosmoTools plugin that may make additional
visualizations like those described above easier to implement.

\subsection{Scaling Studies}
In the long term, one of the bigger questions to determine is how well we can
render this data at scale, in addition to how much data is needed to
meaningfully visualize what is happening (i.e., there is likely to be a point
of diminishing returns where more points just clutter things up or cause a loss
of interactivity that is more harmful). At present, we are working with a
dataset of around 10,000 points, but we hope to scale to 100,000, 1 million,
and 100 million in the future. The sheer difference in size will almost
certainly necessitate different processing of the data, from the amount of data
attached to the sheer method of computation being used to calculate various
parameters from those provided. 3D glyphs will need to be swapped out for 2D
glyphs to maintain some level of interactivity, and it may even be
necessary to par down the amount of data attached to each point 
depending on memory limitations.

\subsection{Different Mediums}
There are a multitude of ways to explore interaction with these
and future visualizations developed. Towards the end of the semester,
we began to try to get X3D exports of the visualizations we developed
working with the Virginia Tech Visionarium. We ran into complications
with the visual being exported as a single giant 3D mesh as opposed
to several smaller ones for each object. Resolving this will would
be a necessary step in further visualization work like this.

In addition to the Visionarium, other mediums like the Oculus Rift
or the Microsoft HoloLens can be explored to see what difference,
if any, these various platforms make in making the data more
intuive and easier to interact with.


\end{document}
