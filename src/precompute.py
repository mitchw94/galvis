'''
This script takes a CSV file of Dr. Farrah's data (necessary
column headers should be evident from the code), and:

1) Outputs a CSV with several additional calculations added
   to the dataset

2) Outputs several CSVs that break the above apart into 
   separate datasets depending on the distance measure
   used (this is useful for, example, viewing the data
   as a time series in ParaView, which automatically 
   recognizes a number of CSV files with the same 
   name as a time series).

@author: Mitch Wagner (mitchw94@vt.edu)

Collaborators: Dr. Nichols Polys (Advisor)
               Dr. Duncan Farrah
'''

import csv
import math
import copy
import numpy as np
import astropy
import astropy.cosmology as cosmo
from astropy.cosmology import LambdaCDM

# The rows I will write out to a CSV file
rows = []

# List of lists of radial distances to the galaxies in the original file
radials = []

# XYZ components of the ultimate vector pointing to the galaxies
xyzeds  = []

# Intended to hold additional parameters derived from the original data
# At the present, only used to hold the Black Hole Mass (BHM) calculation
calcs = []

# Used to initialize CSV DictWriters later on
header = None

# Dark matter cosmology used for the calculations below.
# Duncan knows what the values mean, but they might as
# well be magic numbers...
dark_cosmo = LambdaCDM(H0=70, Om0=0.3, Ode0=0.7)

# These variables will be used to hold the maximum redshift,
# comoving radial distance, and luminosity radial distance calculated
# (redshift can approximate a distance measure, even though it is not
# itself one), for scaling to compare the shapes of the distributions
# that result.
max_zb = 0
maxCo  = 0
maxLum = 0

with open('data.csv', 'rb') as infile:
    reader = csv.DictReader(infile, delimiter=' ', skipinitialspace=True)

    # This data NEEDS to have a header for this to work.
    # The necessary fields are documented below
    header = reader.fieldnames

    # Skip the header column when reading through this
    next(reader)

    for row in reader:
        ''' 
        *** NOTE ***
        For whatever reason, the reader is getting an extra space
        on the end as another row. This is to fix that...
        '''
        row.pop("", None)

        rows.append(row)
        radial = []
        xyzed  = []
        calc = []

        # Get the data of the respective columns
        zb  = np.float32(row['z.b'])
        RAJ = np.float32(row['_RAJ2000'])
        DEJ = np.float32(row['_DEJ2000'])

        co_zb  = dark_cosmo.comoving_distance(zb).value  
        lum_zb = cosmo.luminosity_distance(zb).value

        if zb > max_zb:
            max_zb = zb
        if co_zb > maxCo:
            maxCo = co_zb
        if lum_zb > maxLum:
            maxLum = lum_zb 

        '''
        This block of code calculates the X, Y, and Z coordinates of the
        galaxies described in the CSV file we are reading. We calculate
        distance based purely on using redshift as a distance measure,
        in addition to comoving and luminosity distance measures.

        These are spherical coordinate calculations. Explanations for the
        measures RAJ and DEJ are available online, but they are essentially
        the angles used in such a coordinate system. The comoving distance,
        the luminosity distance, and zb are used as the radial measures. 

        '''

        ########################################################################
        x_no_change = zb * math.sin(math.radians(90 - DEJ)) * math.cos(
                           math.radians(RAJ)) 

        x_co = co_zb * math.sin(math.radians(90 - DEJ)) * math.cos(
                       math.radians(RAJ))       

        x_lum = lum_zb * math.sin(math.radians(90 - DEJ)) * math.cos(
                       math.radians(RAJ))

        ########################################################################

        y_no_change = zb * math.sin(math.radians(90 - DEJ)) * math.sin(
                      math.radians(RAJ)) 

        y_co = co_zb * math.sin(math.radians(90 - DEJ)) * math.sin(
                       math.radians(RAJ)) 

        y_lum = lum_zb * math.sin(math.radians(90 - DEJ)) * math.sin(
                           math.radians(RAJ)) 
        ########################################################################

        z_no_change = zb     * math.cos(math.radians(90 - DEJ))
        z_co        = co_zb  * math.cos(math.radians(90 - DEJ))
        z_lum       = lum_zb * math.cos(math.radians(90 - DEJ))

        ########################################################################

        ''' 
        This is the black hole mass calculation. Lots
        of magic numbers. This calculation should be
        documented elsewhere in this repository.
        '''
        FWCIV = np.float32(row['FW(CIV)'])
        iband = np.float32(row['iMAG'])

        FWCIV = FWCIV / 1000;
        iband = iband + 1.486        

        L1450 = 10**(-.4 * (iband - 4.85))
        L1450 = L1450 * 3.826 * 10**(33)
        L1450 = L1450 / 10**(44)

        result = math.log(FWCIV**2 * (L1450**(.53)), 10) + 6.66
        calc.append(result)

       
        ########################################################################
        '''
        Here, we append everything to be written to the row we eventually
        wish to write out.
        '''

        radial.append(co_zb)
        radial.append(lum_zb)

        xyzed.append(x_no_change)
        xyzed.append(x_co)
        xyzed.append(x_lum)

        xyzed.append(y_no_change)
        xyzed.append(y_co)
        xyzed.append(y_lum)

        xyzed.append(z_no_change)
        xyzed.append(z_co)
        xyzed.append(z_lum)
  
        radials.append(radial)
        xyzeds.append(xyzed)
        calcs.append(calc)

'''
This loop is used to calculate additional distance measures, based on
scaling the luminosity and comoving distributions from the previous 
loop to a range of [0, 1].

We have to do this separately because we had to iterate through
the lists in order to determine the maximum value by which to scale.
'''
for i, row in enumerate(rows): 
    # I need to add scaled zb_lum and zb_co columns, in addition 
    # to xyz for both of them...

    co_scaled_zb = radials[i][0] / maxCo
    lum_scaled_zb = radials[i][1] / maxLum

    RAJ = np.float32(row['_RAJ2000'])
    DEJ = np.float32(row['_DEJ2000'])

    x_co_scaled = co_scaled_zb * math.sin(math.radians(90 - DEJ)) * math.cos(
                     math.radians(RAJ))
    x_lum_scaled = lum_scaled_zb * math.sin(math.radians(90 - DEJ)) * math.cos(
                     math.radians(RAJ)) 
    ##############################################
    y_co_scaled = co_scaled_zb * math.sin(math.radians(90 - DEJ)) * math.sin(
                           math.radians(RAJ)) 
    y_lum_scaled = lum_scaled_zb * math.sin(math.radians(90 - DEJ)) * math.sin(
                           math.radians(RAJ)) 
    ##############################################
    z_co_scaled = co_scaled_zb * math.cos(math.radians(90 - DEJ))
    z_lum_scaled = lum_scaled_zb * math.cos(math.radians(90 - DEJ))

    radials[i].append(co_scaled_zb) 
    radials[i].append(lum_scaled_zb)
    
    xyzeds[i].append(x_co_scaled)
    xyzeds[i].append(x_lum_scaled)
    xyzeds[i].append(y_co_scaled)
    xyzeds[i].append(y_lum_scaled)
    xyzeds[i].append(z_co_scaled)
    xyzeds[i].append(z_lum_scaled)


'''
Here, we write out the previous CSV file data plus our
new calulated values!
'''
with open('all_together.csv', 'wb') as outfile:
    # Get the previous header, and append a bunch of new
    # headers to it as well
    head = list(header)

    # We do this for the same reason we have to deal with
    # that stupid blank space above being counted a row
    head = head[0:len(head) - 1] 
    head.append("co_zb")
    head.append("lum_zb")
    head.append("co_scaled_zb")
    head.append("lum_scaled_zb")

    head.append("x_no_change")
    head.append("x_co")
    head.append("x_lum")
    head.append("y_no_change")
    head.append("y_co")
    head.append("y_lum")
    head.append("z_no_change")
    head.append("z_co")
    head.append("z_lum")

    head.append("x_co_scaled")
    head.append("x_lum_scaled")
    head.append("y_co_scaled")
    head.append("y_lum_scaled")
    head.append("z_co_scaled")
    head.append("z_lum_scaled")   
    head.append("BHM")
  
    writer = csv.DictWriter(outfile, fieldnames=head) 

    writer.writeheader()
    for i, row in enumerate(rows):
        '''
        *** NOTE ***
        I create a deep copy here to avoid adding
        anything to the original row we created, because
        in the next loops, I do NOT want to write out
        all of these parameters!
        '''
        row2 = copy.deepcopy(row)

        row2["co_zb"] = radials[i][0]
        row2["lum_zb"] = radials[i][1] 
        row2["co_scaled_zb"] = radials[i][2]
        row2["lum_scaled_zb"] = radials[i][3]

        row2["x_no_change"] = xyzeds[i][0]
        row2["x_co"] = xyzeds[i][1] 
        row2["x_lum"] = xyzeds[i][2]
 
        row2["y_no_change"] = xyzeds[i][3]
        row2["y_co"] = xyzeds[i][4] 
        row2["y_lum"] = xyzeds[i][5]

        row2["z_no_change"] = xyzeds[i][6]
        row2["z_co"] = xyzeds[i][7] 
        row2["z_lum"] = xyzeds[i][8]

        row2["x_co_scaled"] = xyzeds[i][9]
        row2["x_lum_scaled"] = xyzeds[i][10]
        row2["y_co_scaled"] = xyzeds[i][11]
        row2["y_lum_scaled"] = xyzeds[i][12] 
        row2["z_co_scaled"] = xyzeds[i][13] 
        row2["z_lum_scaled"] = xyzeds[i][14] 
        row2["BHM"] = calcs[i][0] 
        writer.writerow(row2)

#########################################

''' 
These final loops write out the above, but
split into separate files for analysis
as a "time series." Time, here, is used
rather arbitrarily.
'''

header = header[0:len(header) - 1] 
header.append("radial")

header.append("x")
header.append("y")
header.append("z")

with open('out1.csv', 'wb') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=header) 
    writer.writeheader()
    for i, row in enumerate(rows):
        row["radial"] = radials[i][0]

        row["x"] = xyzeds[i][0]
        row["y"] = xyzeds[i][3]
        row["z"] = xyzeds[i][6]

        writer.writerow(row)
        
with open('out2.csv', 'wb') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=header) 
    writer.writeheader()
    for i, row in enumerate(rows):
        row["radial"] = radials[i][0]
        row["x"] = xyzeds[i][2]
        row["y"] = xyzeds[i][5]
        row["z"] = xyzeds[i][8]

        writer.writerow(row)

with open('out3.csv', 'wb') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=header) 
    writer.writeheader()
    for i, row in enumerate(rows):
        row["radial"] = radials[i][1] 

        row["x"] = xyzeds[i][1] 
        row["y"] = xyzeds[i][4] 
        row["z"] = xyzeds[i][7] 

        writer.writerow(row)

with open('out4.csv', 'wb') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=header) 
    writer.writeheader()
    for i, row in enumerate(rows):
        row["radial"] = radials[i][2]

        row["x"] = xyzeds[i][9]
        row["y"] = xyzeds[i][11]
        row["z"] = xyzeds[i][13]

        writer.writerow(row)

with open('out5.csv', 'wb') as outfile:
    writer = csv.DictWriter(outfile, fieldnames=header) 
    writer.writeheader()
    for i, row in enumerate(rows):
        row["radial"] = radials[i][3]

        row["x"] = xyzeds[i][10]
        row["y"] = xyzeds[i][12]
        row["z"] = xyzeds[i][14]

        writer.writerow(row)
